---
title: Exploratory Data Analysis
author: Luke Heley
---


```{r}
library(magrittr)
library(ggplot2)
library(dpe)
```

```{r}

keyword_count <- keywords %>% dplyr::group_by(keyword) %>% dplyr::summarise(n=dplyr::n()) %>%
  dplyr::ungroup() %>% dplyr::filter(keyword != "") %>%
  dplyr::arrange(-n)

ggplot(keyword_count %>% dplyr::filter(n>10)) + 
  geom_bar(aes(reorder(keyword, n), n), stat="identity") + 
  coord_flip()

```


```{r}
author_count <- author_contact %>% dplyr::group_by(author) %>% dplyr::summarise(n=dplyr::n()) %>%
  dplyr::ungroup() %>% dplyr::filter(author != "") %>%
  dplyr::arrange(-n) %>%
  dplyr::mutate(author = trimws(author))

ggplot(author_count %>% dplyr::filter(n>=5)) + 
  geom_bar(aes(reorder(author, n), n), stat="identity") + 
  coord_flip()

```
